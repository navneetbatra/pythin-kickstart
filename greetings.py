"""
A way to write comment in python
"""
'''
A way to write comment in python
'''
# Another way to write comment
from curses.ascii import ETB


def compose(func1, func2):
    def composed_func(x):
        return func1(func2(x))
    return composed_func

def apply(func, x):
    return func(x)

def square(x):
    return x*x

methods = {'square' : square, 'quad' : compose(square, square)}
print(apply(square, 3))

for method in methods.values():
    print(method(2))