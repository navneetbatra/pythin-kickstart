from concurrent.futures import process


operationsDict = {"+" : lambda x,y : x + y, "-" : lambda x,y : x - y, "x" : lambda x,y : x * y, "/" : lambda x,y : x / y, "^" : lambda x,y : x**y, "%" : lambda x,y : x%y}

def calculatorStep1():
    operation = input("Enter an operation: ")
    parameterA = int(input("Enter 1st parameter: "))
    parameterB = int(input("Enter 2nd parameter: "))

    print(operationsDict[operation](parameterA, parameterB))

def calculatorStep2():
    with open("calc1.txt", "r") as file:
        text_string = file.read().splitlines()

    print (text_string)
    result = 0
    for item in text_string:
        split = item.split(' ')
        operation = split[1]
        parameterA = int(split[2])
        parameterB = int(split[3])
        result += operationsDict[operation](parameterA, parameterB)
    print(result)

def calculatorStep3():
    with open("calc2.txt", "r") as file:
        text_string = file.read().splitlines()

    result = 0
    processedLines = []
    breakLine = False
    for item in text_string:
        split = item.split(' ')
        while(not breakLine and split[1] != "calc"):
            index = int(split[1])
            line = text_string[index]
            if (line in processedLines):
                print("Line: " + str(index) + " Instruction: " + line)
                breakLine = True
                break
            processedLines.append(line)
            split = text_string[index].split(' ')
        if (not breakLine):
            operation = split[2]
            parameterA = int(split[3])
            parameterB = int(split[4])
            result += operationsDict[operation](parameterA, parameterB)

    if (not breakLine):
        print(result)


def calculatorStep4():
    with open("calc3.txt", "r") as file:
        text_string = file.read().splitlines()

    result = 0
    processedLines = []
    breakLine = False
    for item in text_string:
        split = item.split(' ')
        while(not breakLine and split[1] != "calc"):
            index = int(split[1])
            line = text_string[index]
            if (line in processedLines):
                print("Line: " + str(index) + " Instruction: " + line)
                breakLine = True
                break
            processedLines.append(line)
            split = text_string[index].split(' ')
        if (not breakLine):
            operation = split[2]
            parameterA = int(split[3])
            parameterB = int(split[4])
            result += operationsDict[operation](parameterA, parameterB)

    if (not breakLine):
        print(result)

calculatorStep1()
    